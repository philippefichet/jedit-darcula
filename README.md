# Darcula LookAndFeel Plugin to jEdit

## How to build

`./gradlew shadowJar`

## How to install

Install LookAndFeel JEdit plugin into plugin repository

Copy-paste `build\libs\jedit-darcula-gradle-all.jar` (or download [jedit-darcula-gradle-all.jar](https://bitbucket.org/philippefichet/jedit-darcula/downloads/jedit-darcula-gradle-all.jar) into jEdit home "jars" folder.

- Windows : `~\AppData\Roaming\jEdit\jars`
- Linux : `~/jEdit/jars`