package darcula;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import com.bulenkov.darcula.DarculaLaf;
import lookandfeel.LookAndFeelInstaller;
import org.gjt.sp.jedit.AbstractOptionPane;

public class DarculaLookAndFeelInstaller implements LookAndFeelInstaller {

    public String getName() {
        return "Darcula";
    }

    /**
     * Install a non standard look and feel.
     */
    public void install() throws UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel(new DarculaLaf());
        UIManager.put("ClassLoader", DarculaLaf.class.getClassLoader());
    }

    public AbstractOptionPane getOptionPane() {
        return null;
    }
}
